<?php
namespace Avanti\ContactSendEmail\Model;

use Avanti\ContactSendEmail\Model\Mail\TransportBuilder;
use Avanti\ContactSendEmail\Model\ConfigInterface;
use Avanti\ContactSendEmail\Model\MailInterface;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Area;
use Magento\MediaStorage\Model\File\UploaderFactory;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Filesystem\DirectoryList;

class Mail implements MailInterface
{
    /**
     * @var TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var StateInterface
     */
    private $inlineTranslation;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var UploaderFactory
     */
    private $uploader;

    /**
     * @var Filesystem
     */
    private $fileSystem;

    /**
     * @var ConfigInterface
     */
    private $contactsConfig;

    /**
     * Initialize dependencies.
     *
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     * @param UploaderFactory $uploader
     * @param Filesystem $fileSystem
     * @param ConfigInterface $contactsConfig
     * @param StoreManagerInterface|null $storeManager
     */
    public function __construct(
        TransportBuilder $transportBuilder,
        StateInterface $inlineTranslation,
        UploaderFactory $uploader,
        Filesystem $fileSystem,
        ConfigInterface $contactsConfig,
        StoreManagerInterface $storeManager = null

    ) {
        $this->transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->uploader = $uploader;
        $this->fileSystem = $fileSystem;
        $this->contactsConfig = $contactsConfig;
        $this->storeManager = $storeManager ?: ObjectManager::getInstance()->get(StoreManagerInterface::class);
    }

    /**
     * Send email from contact form
     *
     * @param string $replyTo
     * @param array $variables
     * @return void
     */
    public function send($replyTo, array $variables)
    {
        /** @see \Magento\Contact\Controller\Index\Post::validatedParams() */
        $replyToName = !empty($variables['data']['name']) ? $variables['data']['name'] : null;

        $emailRecipient = $this->getEmailRecipient();

        $this->inlineTranslation->suspend();
        try {
            $transport = $this->transportBuilder
                ->setTemplateIdentifier('avanti_work_with_us')
                ->setTemplateOptions(
                    [
                        'area' => Area::AREA_FRONTEND,
                        'store' => $this->storeManager->getStore()->getId()
                    ]
                )
                ->setTemplateVars($variables)
                ->setFrom($this->contactsConfig->emailSender())
                ->addTo($emailRecipient)
                ->setReplyTo($replyTo);

            if ($variables['data']['upload_file']) {
                $uploader = $this->uploader->create(['fileId' => 'upload_file']);
                $uploader->setFilesDispersion(false);
                $uploader->setFilenamesCaseSensitivity(false);
                $uploader->setAllowRenameFiles(true);

                $mediaDirectory = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA);
                $absolutePath = $mediaDirectory->getAbsolutePath();
                $result = $uploader->save($absolutePath);

                $file = $result['path'] . $result['file'];
                $transport->addAttachment(file_get_contents($file), $result['file'], $result['type']);
            }

            $transport = $transport->getTransport();
            $transport->sendMessage();
        } finally {
            $this->inlineTranslation->resume();
        }
    }


    private function getEmailRecipient()
    {
        return $this->contactsConfig->emailRecipient();
    }
}
