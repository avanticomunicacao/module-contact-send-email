<?php
declare(strict_types=1);

namespace Avanti\ContactSendEmail\Controller\Send;

use Avanti\ContactSendEmail\Model\MailInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Setup\Exception;
use Psr\Log\LoggerInterface;
use Magento\Framework\Controller\ResultFactory;


class Email extends Action implements HttpPostActionInterface
{
    const TYPE_DOCX = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    const TYPE_DOC = 'application/msword';
    const TYPE_PDF = 'application/pdf';

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var MailInterface
     */
    protected $mail;

    /**
     * Constructor
     *
     * @param Context $context
     * @param LoggerInterface $logger
     * @param MailInterface $mail
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        MailInterface $mail
    ) {
        parent::__construct($context);
        $this->logger = $logger;
        $this->mail = $mail;
    }

    /**
     * Execute view action
     */
    public function execute()
    {
        try {
            $this->sendEmail($this->validatedParams());
            $this->messageManager->addSuccessMessage(
                __('We received your message. We will contact you if necessary.')
            );
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Unable to receive your message. Try later.'));
        }

        return $this->resultRedirectFactory->create()->setPath('trabalhe-conosco');
    }

    /**
     * Return true or false if the file is compatible to be sent
     * @param $file
     * @return bool
     */
    private function validateFile($file)
    {
        $types = array(self::TYPE_DOC, self::TYPE_DOCX, self::TYPE_PDF);
        $type = $file['upload_file']['type'];

        $return = array_search($type, $types);

        if ($return) {
            return true;
        }

        return false;
    }

    /**
     * @param array $post Post data from contact form
     * @return void
     */
    private function sendEmail($post)
    {
        $result = $post->getParams();
        $result['subject'] = "Contato - Trabalhe Conosco";

        try {
            if ($post->getFiles()) {
                $files = $post->getFiles()->getArrayCopy();
                if ($files['upload_file']['size'] > 0) {
                    $result = array_merge($files, $result);
                    if (!$this->validateFile($files)) {
                       throw new Exception("This file is not supported");
                    }
                }

                $this->mail->send(
                    $result['email'],
                    ['data' => new DataObject($result)]
                );
            }
        } catch (\Exception $e) {
            $this->logger->debug("Erro in send the message.");
            $this->logger->debug($e);
            throw new Exception("Error in sent the message");
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function validatedParams()
    {
        $request = $this->getRequest();
        if (trim($request->getParam('name')) === '') {
            throw new LocalizedException(__('Enter the Name and try again.'));
        }
        if (false === \strpos($request->getParam('email'), '@')) {
            throw new LocalizedException(__('The email address is invalid. Verify the email address and try again.'));
        }
        if (trim($request->getParam('comment')) === '') {
            throw new LocalizedException(__('Enter the message and try again.'));
        }

        return $request;
    }
}
